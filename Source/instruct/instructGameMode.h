// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "instructGameMode.generated.h"

UCLASS(MinimalAPI)
class AinstructGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AinstructGameMode();
};



